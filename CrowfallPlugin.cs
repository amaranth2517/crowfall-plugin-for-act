﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Advanced_Combat_Tracker;

[assembly: AssemblyTitle("CrowfallPlugin")]
[assembly: AssemblyDescription("Crowfall Plugin for ACT")]
[assembly: AssemblyCopyright("By Amaranth © 2020")]
[assembly: AssemblyVersion("1.0.0.0")]

namespace CrowfallPlugin
{
    public class CrowfallPlugin : UserControl, IActPluginV1
    {
        private readonly IContainer components = null;
        private Label _lblStatus;
        private readonly string _settingsFile = Path.Combine(ActGlobals.oFormActMain.AppDataFolder.FullName, "Config\\CrowfallPlugin.config.xml");
        private SettingsSerializer _xmlSettings;

        private string _language;

        private readonly Dictionary<string, Dictionary<string, string>> _contexts = new Dictionary<string, Dictionary<string, string>>
        {
            {
                "en", new Dictionary<string, string>
                {
                    {"healed", "healed"},
                    {"Unknown", "Practice Dummy"},
                    {"Lifesteal", "Lifesteal"},
                    {"Your", "Your"},
                    {"Fall", "Fall"},
                    {"Berserk", "Berserk Crush"},
                    {"Essence Burn", "Essence Burn"},
                    {"You", "You"},
                    {"(Critical)", "(Critical)"},
                }
            },
            {
                "ru", new Dictionary<string, string>
                {
                    {"healed", "исцеляет цель"},
                    {"Unknown", "Practice Dummy"}, // todo: translate
                    {"Lifesteal", "Lifesteal"},
                    {"Your", "Ваш"},
                    {"Fall", "Падение"},
                    {"Berserk", "Berserk Crush"},
                    {"Essence Burn", "Essence Burn"},
                    {"You", "Вы"},
                    {"(Critical)", "(Критический удар)"},
                }
            }
        };

        private Dictionary<string, string> _context; // todo: replace with variables for performance?

        private static readonly Regex EnRegex = new Regex(@"(.*) INFO    COMBAT    - Combat _\|\|_ Event=\[(Your|\w*)(.*) (hit|healed|whiffed) (You|.*).*for (\d*)(.*)\.\]");

        private static readonly Regex
            RuRegex = new Regex("(.*) INFO    COMBAT    - Combat _\\|\\|_ Event=\\[(Ваш|.*): \"(.*)\" (поражает цель|исцеляет цель|whiffed) (Вы|.*) на (\\d*)(.*)\\.\\]"); // todo: whiffed?

        private readonly Dictionary<string, Regex> _regexes = new Dictionary<string, Regex> {{"en", EnRegex}, {"ru", RuRegex}};

        private Regex _regex = EnRegex;

        public CrowfallPlugin()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SuspendLayout();

            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            Name = "CrowfallPlugin";
            Size = new Size(686, 384);

            ResumeLayout(false);
            PerformLayout();
        }

        public void InitPlugin(TabPage pluginScreenSpace, Label pluginStatusText)
        {
            _lblStatus = pluginStatusText;
            pluginScreenSpace.Controls.Add(this);
            Dock = DockStyle.Fill;

            _xmlSettings = new SettingsSerializer(this);
            LoadSettings();
           
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var logsDirectory = appData + @"Low\Art+Craft\Crowfall\CombatLogs";

            var directoryInfo = new DirectoryInfo(logsDirectory);
            if (!directoryInfo.Exists)
            {
                ExitACT();
                return;
            }

            var log = directoryInfo.GetFiles().OrderByDescending(x => x.LastWriteTime).FirstOrDefault();

            if (log == null)
            {
                ExitACT();
                return;
            }

            ActGlobals.oFormActMain.LogFilePath = log.FullName;
            ActGlobals.oFormActMain.LogFileFilter = "*.log";
            ActGlobals.oFormActMain.LogPathHasCharName = false;
            ActGlobals.oFormActMain.OnLogLineRead += ParseLine;
            ActGlobals.oFormActMain.GetDateTimeFromLog = ParseDateTime;
            ActGlobals.oFormActMain.LogEncoding = Encoding.UTF8;

            // todo: Check update button

            _lblStatus.Text = "Crowfall Plugin (by Amaranth) Started";
        }

        private static void ExitACT()
        {
            MessageBox.Show("Combat logs are not enabled. Please enable logs in Crowfall settings, hit a dummy and start ACT again.");
            Application.Exit();
            return;
        }

        public void DeInitPlugin()
        {
            // ReSharper disable once DelegateSubtraction
            ActGlobals.oFormActMain.GetDateTimeFromLog -= ParseDateTime;
            // ActGlobals.oFormActMain.UpdateCheckClicked -= UpdateCheckClicked;
            ActGlobals.oFormActMain.OnLogLineRead -= ParseLine;


            SaveSettings();
            _lblStatus.Text = "Crowfall Plugin (by Amaranth) Exited";
        }

        private void LoadSettings()
        {
            // _xmlSettings.AddControlSetting(textBox1.Name, textBox1);

            if (File.Exists(_settingsFile))
            {
                FileStream fs = new FileStream(_settingsFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlTextReader xReader = new XmlTextReader(fs);

                try
                {
                    while (xReader.Read())
                    {
                        if (xReader.NodeType == XmlNodeType.Element)
                        {
                            if (xReader.LocalName == "SettingsSerializer")
                            {
                                _xmlSettings.ImportFromXml(xReader);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _lblStatus.Text = "Error loading settings: " + ex.Message;
                }

                xReader.Close();
            }
        }

        void SaveSettings()
        {
            var fs = new FileStream(_settingsFile, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            var xWriter = new XmlTextWriter(fs, Encoding.UTF8)
            {
                Formatting = Formatting.Indented, Indentation = 1, IndentChar = '\t'
            };
            xWriter.WriteStartDocument(true);
            xWriter.WriteStartElement("Config"); // <Config>
            xWriter.WriteStartElement("SettingsSerializer"); // <SettingsSerializer>
            _xmlSettings.ExportToXml(xWriter);
            xWriter.WriteEndElement(); // </SettingsSerializer>
            xWriter.WriteEndElement(); // </Config>
            xWriter.WriteEndDocument();
            xWriter.Flush();
            xWriter.Close();
        }

        private DateTime ParseDateTime(string line)
        {
            try
            {
                var match = _regex.Match(line);

                return DateTime.Parse(match.Groups[1].Value);
            }
            catch
            {
                return ActGlobals.oFormActMain.LastEstimatedTime;
            }
        }

        private void ParseLine(bool isImport, LogLineEventArgs log)
        {
            ActGlobals.oFormActMain.GlobalTimeSorter++;
            var line = log.logLine;

            if (_language == null && !DetectLanguage(line))
                return; // todo: notify unsupported language

            var match = _regex.Match(line);
            if (!match.Success)
                return;

            var timestamp = ActGlobals.oFormActMain.LastKnownTime;

            var damage = match.Groups[4].Value != _context["healed"];
            var skillName = match.Groups[3].Value.Trim();
            var skill = string.IsNullOrEmpty(skillName) ? (damage ? _context["Unknown"] : _context["Lifesteal"]) : skillName;
            var character = match.Groups[2].Value;
            var selfDamage = skill == _context["Fall"] || skill == _context["Berserk"] || skill == _context["Essence Burn"];
            var mine = character == _context["Your"] && !selfDamage;
            var target = string.IsNullOrEmpty(match.Groups[5].Value) ? _context["Unknown"] : match.Groups[5].Value;
            var me = target == _context["You"];
            var amount = int.Parse(match.Groups[6].Value);
            var critical = match.Groups[7].Value.Contains(_context["(Critical)"]);
            var damageType = match.Groups[7].Value.Replace("(Critical)", "").Trim();

            if (isImport && ActGlobals.oFormActMain.CurrentZone == "Import Zone" || ActGlobals.oFormActMain.CurrentZone == "Unknown Zone")
                ActGlobals.oFormActMain.ChangeZone("Crowfall");

            ActGlobals.charName = "You";
            UpdateFight(mine, damage, skill, selfDamage ? skill : (mine ? target : character), amount, critical, me, timestamp, damageType);
        }

        private bool DetectLanguage(string line)
        {
            foreach (var regex in _regexes.Where(regex => regex.Value.Match(line).Success))
            {
                _regex = regex.Value;
                _language = regex.Key;
                _context = _contexts[_language];
                return true;
            }

            return false;
        }

        private const int Dmg = 2, Heals = 3;

        private static void UpdateFight(bool mine, bool damage, string skill, string opponent, int amount, bool critical, bool me, DateTime timestamp, string damageType)
        {
            if (mine)
            {
                if (ActGlobals.oFormActMain.SetEncounter(timestamp, ActGlobals.charName, opponent))
                    ActGlobals.oFormActMain.AddCombatAction(damage ? Dmg : Heals, critical, null, ActGlobals.charName, skill,
                        new Dnum(amount), timestamp, ActGlobals.oFormActMain.GlobalTimeSorter, opponent, damageType);
            }
            else
            {
                if (ActGlobals.oFormActMain.SetEncounter(timestamp, opponent, ActGlobals.charName))
                    ActGlobals.oFormActMain.AddCombatAction(damage ? Dmg : Heals, critical, null, opponent, skill,
                        new Dnum(amount), timestamp, ActGlobals.oFormActMain.GlobalTimeSorter, ActGlobals.charName, damageType);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        private static void Log(string log)
        {
            using (var streamWriter = File.AppendText("C:\\Users\\Amaranth\\Desktop\\log.txt"))
            {
                streamWriter.WriteLine(log);
                streamWriter.Close();
            }
        }
    }
}